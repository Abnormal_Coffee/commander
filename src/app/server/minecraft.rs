use std::thread::spawn;

use crossbeam_channel::{Receiver, Sender};
use eframe::{
    egui::{Button, Context, Grid, TextEdit, Ui},
    epaint::Vec2,
};

use self::{client::Client, player::Player};

mod client;
mod player;

#[derive(serde::Deserialize, serde::Serialize, Default)]
pub struct MinecraftServer {
    log: Vec<String>,
    players: Vec<Player>,
    command_input: String,
    log_filter: String,
    #[serde(skip)]
    client_send_receive: Option<(Sender<String>, Receiver<String>)>,
}

impl Clone for MinecraftServer {
    fn clone(&self) -> Self {
        MinecraftServer {
            client_send_receive: None,
            ..Clone::clone(&self)
        }
    }
}

impl MinecraftServer {
    pub fn ui(&mut self, ui: &mut Ui, address: String, password: String, ctx: &Context) {
        let widget_size = Vec2 { x: 128., y: 16. };

        let mut clear_channel = false;

        if let Some((sender, receiver)) = &self.client_send_receive {
            match receiver.try_recv() {
                Ok(message) => self.log.push(message),
                Err(err) => {}
            }
            ui.horizontal(|ui| {
                if ui
                    .add(Button::new("Reload Client").min_size(Vec2 { x: 128., y: 16. }))
                    .clicked()
                {
                    clear_channel = true;
                }
                if ui
                    .add(
                        TextEdit::singleline(&mut self.command_input)
                            .hint_text("Command Input")
                            .desired_width(f32::INFINITY),
                    )
                    .lost_focus()
                    && ui.input(|i| i.key_pressed(eframe::egui::Key::Enter))
                {
                    _ = sender.send(self.command_input.clone());
                    self.command_input.clear();
                };
            });

            ui.horizontal(|ui| {
                if ui
                    .add(Button::new("Clear").min_size(Vec2 { x: 128., y: 16. }))
                    .clicked()
                {
                    self.log.clear()
                }
                ui.add(
                    TextEdit::singleline(&mut self.log_filter)
                        .hint_text("Filter")
                        .desired_width(f32::INFINITY),
                );
            });

            Grid::new("Log").striped(true).show(ui, |ui| {
                self.log
                    .iter()
                    .filter(|log| log.contains(&self.log_filter))
                    .for_each(|log| {
                        ui.label(log);
                        ui.end_row()
                    })
            });
        } else {
            let (client_send, client_receive): (Sender<String>, Receiver<String>) =
                crossbeam_channel::unbounded();
            let (command_send, command_receiver): (Sender<String>, Receiver<String>) =
                crossbeam_channel::unbounded();
            self.client_send_receive = Some((command_send, client_receive));
            spawn(move || {
                println!("Spawned");
                if let Ok(client) = Client::new(address) {
                    println!("Client");
                    let mut client = client;
                    let client_result = client.authenticate(password);
                    if let Err(client_result) = client_result {
                        _ = client_send.send(client_result.to_string());
                    } else {
                        _ = client_send.send("Conntected to server".to_string());
                        'sendrecv: loop {
                            match command_receiver.try_recv() {
                                Ok(command) => {
                                    if let Ok(message) = client.send_command(command) {
                                        _ = client_send.send(message.body);
                                    };
                                }
                                Err(err) => {
                                    if err.is_disconnected() {
                                        break 'sendrecv;
                                    }
                                }
                            }
                        }
                    }
                };
            });
        }

        if clear_channel == true {
            self.client_send_receive = None;
            ui.label("Failed to connect to client");
        }
    }
}
