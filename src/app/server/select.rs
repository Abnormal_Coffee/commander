use eframe::egui::Ui;

use super::{minecraft::MinecraftServer, ServerType};

pub fn server_select(ui: &mut Ui) -> ServerType {
    if ui.add(eframe::egui::Button::new("Minecraft")).clicked() {
        return ServerType::MinecraftServer(MinecraftServer::default());
    }
    ServerType::Select
}
