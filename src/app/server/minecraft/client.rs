// Taken straight from minecraft-client-rs:
// https://github.com/willroberts/minecraft-client-rs

use std::convert::TryInto;
use std::error::Error;
use std::fmt;
use std::io::prelude::*;
use std::net::{Shutdown, TcpStream};
use std::str::from_utf8;
use std::sync::atomic::{AtomicI32, Ordering};

const MAX_MESSAGE_SIZE: usize = 4110; // https://wiki.vg/Rcon#Fragmentation

#[derive(Debug)]
struct RequestIDMismatchError;

impl Error for RequestIDMismatchError {}

impl fmt::Display for RequestIDMismatchError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "received error response from server")
    }
}

pub struct Client {
    conn: TcpStream,
    last_id: AtomicI32,
}

impl Client {
    pub fn new(hostport: String) -> Result<Client, Box<dyn Error>> {
        let conn = TcpStream::connect(hostport)?;
        Ok(Client {
            conn: conn,
            last_id: AtomicI32::new(0),
        })
    }

    pub fn close(&mut self) -> Result<(), Box<dyn Error>> {
        self.conn.shutdown(Shutdown::Both)?;
        Ok(())
    }

    pub fn authenticate(&mut self, password: String) -> Result<Message, Box<dyn Error>> {
        self.send_message(MessageType::Authenticate as i32, password)
    }

    pub fn send_command(&mut self, command: String) -> Result<Message, Box<dyn Error>> {
        self.send_message(MessageType::Command as i32, command)
    }

    fn next_id(&self) -> i32 {
        let prev = self.last_id.load(Ordering::Relaxed);
        let next = prev + 1;
        self.last_id.store(next, Ordering::Relaxed);
        next
    }

    fn send_message(&mut self, msg_type: i32, msg_body: String) -> Result<Message, Box<dyn Error>> {
        let req_id = self.next_id();
        let req = Message {
            size: msg_body.len() as i32 + HEADER_SIZE,
            id: req_id.clone(),
            msg_type: msg_type,
            body: msg_body,
        };

        self.conn.write_all(&encode_message(req)[..])?;
        let mut resp_bytes = [0u8; MAX_MESSAGE_SIZE];
        self.conn.read(&mut resp_bytes)?;
        let resp = decode_message(resp_bytes.to_vec())?;

        if req_id == resp.id {
            Ok(resp)
        } else {
            Err(Box::new(RequestIDMismatchError))
        }
    }
}

pub const HEADER_SIZE: i32 = 10;

#[repr(i32)]
pub enum MessageType {
    Response,
    _Unused,
    Command,
    Authenticate,
}

#[derive(Debug)]
pub struct Message {
    pub size: i32,
    pub id: i32,
    pub msg_type: i32,
    pub body: String,
}

pub fn encode_message(msg: Message) -> Vec<u8> {
    let mut bytes: Vec<u8> = vec![];

    bytes.extend_from_slice(&msg.size.to_le_bytes());
    bytes.extend_from_slice(&msg.id.to_le_bytes());
    bytes.extend_from_slice(&msg.msg_type.to_le_bytes());
    bytes.extend_from_slice(msg.body.as_bytes());
    bytes.extend_from_slice(&[0, 0]);

    bytes
}

pub fn decode_message(bytes: Vec<u8>) -> Result<Message, Box<dyn Error>> {
    let size = i32::from_le_bytes(bytes[0..4].try_into()?);
    let id = i32::from_le_bytes(bytes[4..8].try_into()?);
    let msg_type = i32::from_le_bytes(bytes[8..12].try_into()?);

    let mut body = "".to_string();
    let body_len: usize = (size - HEADER_SIZE).try_into()?;
    if body_len > 0 {
        let body_bytes = from_utf8(&bytes[12..12 + body_len])?;
        body = body_bytes.to_string();
    }

    Ok(Message {
        size: size,
        id: id,
        msg_type: msg_type,
        body: body,
    })
}

// This part I did myself - not sure if I should have though
impl Drop for Client {
    fn drop(&mut self) {
        _ = self.close();
    }
}
