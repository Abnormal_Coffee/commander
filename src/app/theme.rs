use catppuccin_egui::*;
use eframe::egui::Context;

#[derive(serde::Deserialize, serde::Serialize, Default, PartialEq)]
pub enum Themer {
    Dark,
    Light,
    #[default]
    Nord,
    Latte,
    Frappe,
    Macchiato,
    Mocha,
}

impl Themer {
    pub fn themer(self: &Self, ctx: &Context) {
        match self {
            Self::Dark => ctx.set_visuals(eframe::Theme::Dark.egui_visuals()),
            Self::Light => ctx.set_visuals(eframe::Theme::Light.egui_visuals()),
            Self::Nord => ctx.set_style(egui_nord::style()),
            Self::Latte => set_theme(ctx, LATTE),
            Self::Frappe => set_theme(ctx, FRAPPE),
            Self::Macchiato => set_theme(ctx, MACCHIATO),
            Self::Mocha => set_theme(ctx, MOCHA),
        }
    }
    pub fn select(self: &mut Self, ctx: &Context, open: &mut bool) {
        eframe::egui::Window::new("Theme Settings").show(ctx, |ui| {
            let responses = [
                ui.selectable_value(self, Self::Dark, "Dark"),
                ui.selectable_value(self, Self::Light, "Light"),
                ui.selectable_value(self, Self::Nord, "Nord"),
                ui.selectable_value(self, Self::Latte, "Latte"),
                ui.selectable_value(self, Self::Frappe, "Frappe"),
                ui.selectable_value(self, Self::Macchiato, "Macchiato"),
                ui.selectable_value(self, Self::Mocha, "Mocha"),
            ];
            responses.iter().for_each(|response| {
                if response.clicked() {
                    self.themer(ctx);
                }
            });
            if ui.button("Close").clicked() {
                *open = false;
            }
        });
    }
}
