use eframe::egui::{Context, RichText, Ui, WidgetText};

use self::select::server_select;

mod minecraft;
mod select;

#[derive(serde::Deserialize, serde::Serialize, Default, Clone)]
pub struct Server {
    pub address: String,
    password: String,
    pub name: String,
    pub server_type: ServerType,
}
#[derive(serde::Deserialize, serde::Serialize, Default, Clone)]
pub enum ServerType {
    #[default]
    Select,
    MinecraftServer(minecraft::MinecraftServer),
}

impl Server {
    pub fn title(&mut self) -> WidgetText {
        // let title = RichText::new(format!("{}@{}", self.name, self.address));
        return WidgetText::RichText(RichText::new("broken"));
    }
    pub fn add_server(self: &mut Self, ctx: &Context) -> Option<Server> {
        let mut ret = None;
        eframe::egui::Window::new("Add Server").show(ctx, |ui| {
            ui.add(
                eframe::egui::TextEdit::singleline(&mut self.name)
                    .hint_text("Server Name")
                    .desired_width(f32::INFINITY),
            );
            ui.add(
                eframe::egui::TextEdit::singleline(&mut self.address)
                    .hint_text("Server Address")
                    .desired_width(f32::INFINITY),
            );
            ui.add(
                eframe::egui::TextEdit::singleline(&mut self.password)
                    .password(true)
                    .hint_text("Server Password")
                    .desired_width(f32::INFINITY),
            );
            if ui.button("Finish").clicked() {
                ret = Some(self.clone());
            };
        });
        ret
    }
    pub fn ui(self: &mut Self, ui: &mut Ui, ctx: &Context) {
        // let widget_size = Vec2 { x: 128., y: 16. };
        ui.add(
            eframe::egui::TextEdit::singleline(&mut self.name)
                .hint_text("Server Name")
                .desired_width(f32::INFINITY),
        );
        ui.add(
            eframe::egui::TextEdit::singleline(&mut self.address)
                .hint_text("{Server Address}:{Port}")
                .desired_width(f32::INFINITY),
        );
        ui.add(
            eframe::egui::TextEdit::singleline(&mut self.password)
                .password(true)
                .hint_text("Server Password")
                .desired_width(f32::INFINITY),
        );
        ui.separator();
        match &mut self.server_type {
            ServerType::Select => self.server_type = server_select(ui),
            ServerType::MinecraftServer(server) => {
                server.ui(ui, self.address.clone(), self.password.clone(), ctx)
            }
        }
    }
}
