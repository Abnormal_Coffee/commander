use eframe::{egui::RichText, epaint::Vec2};
use egui_dock::Tree;

use self::{server::Server, theme::Themer};

mod server;
mod theme;

pub struct App {
    appstate: AppState,
}

impl Default for App {
    fn default() -> Self {
        Self {
            appstate: AppState::default(),
        }
    }
}

impl App {
    pub fn new(cc: &eframe::CreationContext<'_>) -> Self {
        if let Some(storage) = cc.storage {
            let app = App {
                appstate: eframe::get_value(storage, eframe::APP_KEY)
                    .unwrap_or_else(|| AppState::default()),
            };
            app.appstate.theme.0.themer(&cc.egui_ctx);
            return app;
        } else {
            App::default()
        }
    }
}

#[derive(serde::Deserialize, serde::Serialize, Default)]
#[serde(default)]
pub struct AppState {
    add_server: Option<Server>,
    tree: Tree<Server>,
    theme: (Themer, bool),
}

impl eframe::App for App {
    fn save(&mut self, storage: &mut dyn eframe::Storage) {
        eframe::set_value(storage, eframe::APP_KEY, &mut self.appstate);
    }

    fn update(&mut self, ctx: &eframe::egui::Context, _frame: &mut eframe::Frame) {
        let Self { appstate } = self;
        let AppState {
            add_server,
            tree,
            theme,
        } = appstate;

        eframe::egui::CentralPanel::default().show(ctx, |ui| {
            if theme.1 == true {
                theme.0.select(ctx, &mut theme.1);
            }

            if tree.is_empty() && add_server.is_none() {
                *add_server = Some(Server::default())
            }

            if let Some(server) = add_server {
                if let Some(server) = server.add_server(ctx) {
                    tree.push_to_first_leaf(server);
                    *add_server = None;
                }
            }

            let mut tab_viewer = TabViewer {
                add_button: AddButton::None,
            };

            egui_dock::DockArea::new(tree)
                .show_add_buttons(true)
                .show_add_popup(true)
                .style(egui_dock::Style::from_egui(ui.style().as_ref()))
                .show_inside(ui, &mut tab_viewer);

            match tab_viewer.add_button {
                AddButton::ThemeSettings => theme.1 = true,
                AddButton::AddServer => *add_server = Some(Server::default()),
                _ => {}
            }
        });
    }
}

struct TabViewer {
    add_button: AddButton,
}

enum AddButton {
    None,
    AddServer,
    ThemeSettings,
}

impl egui_dock::TabViewer for TabViewer {
    type Tab = Server;

    fn ui(&mut self, ui: &mut eframe::egui::Ui, tab: &mut Self::Tab) {
        let ctx = ui.ctx().clone();
        tab.ui(ui, &ctx);
    }

    fn title(&mut self, tab: &mut Self::Tab) -> eframe::egui::WidgetText {
        tab.title()
    }

    fn add_popup(&mut self, ui: &mut eframe::egui::Ui, _node: egui_dock::NodeIndex) {
        if ui
            .add_sized(
                Vec2 { x: 64., y: 16. },
                eframe::egui::Button::new("Theme Settings"),
            )
            .clicked()
        {
            self.add_button = AddButton::ThemeSettings
        };
        if ui
            .add_sized(
                Vec2 { x: 64., y: 16. },
                eframe::egui::Button::new("Add Server"),
            )
            .clicked()
        {
            self.add_button = AddButton::AddServer
        };
    }
}
