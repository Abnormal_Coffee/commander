# Commander - TBD

## Installation
### Manual:
- Install the rustup toolchain
- Download the repo using `git clone https://gitlab.com/Abnormal_Coffee/commander`
- Open a terminal and navigate to the repo `cd ~/PATH/TO/REPO/`
- Compile with `cargo build --release`
- Execute with `./targets/release/commander` (`commander.exe` on windows)
### Crates.io:
- `cargo install commander`
### Webapp:
- Navigate to the webapp, then optionally install as a pwa